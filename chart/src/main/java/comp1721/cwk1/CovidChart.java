package comp1721.cwk1;

// Implement your solution to the Advanced task here
// (Note: the class must be named CovidChart)

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.io.IOException;

import static java.lang.System.out;

public class CovidChart extends Application {

  //read from specified file and convert data into format to use for the graph
  //generate the window and display the graph
  public void start(Stage stage)
  {
    try
    {
      int activeStaff;
      int activeStudents;
      int activeOther;
      int totalActiveCases;
      int dayOfYear;
      //get filename and read into newDataset
      String filename = getParameters().getUnnamed().get(0);
      CovidDataset graphDataset = new CovidDataset();
      graphDataset.readDailyCases(filename);

      stage.setTitle("COMP1721 Coursework 1");

      //create and label x and y axes
      final NumberAxis xAxis = new NumberAxis();
      final NumberAxis yAxis = new NumberAxis();
      xAxis.setLabel("Day of Year");
      yAxis.setLabel("Active Cases");

      //start x axes from smallest value and not 0
      xAxis.setForceZeroInRange(false);

      //create a new LineChart and title it
      LineChart<Number,Number> lineChart = new LineChart<Number,Number>(xAxis,yAxis);
      lineChart.setTitle("Active Coronavirus Cases, University of Leeds");
      //create a series of data to later assign the data to the graph
      XYChart.Series series = new XYChart.Series();
      series.setName(filename);

      //populate the data series by iterate through each dataset, skipping the first 9 days
      for (int i = 9; i < graphDataset.size(); i++)
      {
        //reset active case values
        activeStaff = 0;
        activeStudents = 0;
        activeOther = 0;

        //iterate through the past 10 days
        for (int j = i; j > (i - 10); j--)
        {
          //sum the active cases
          activeStaff = activeStaff + graphDataset.getRecord(j).getStaffCases();
          activeStudents = activeStudents + graphDataset.getRecord(j).getStudentCases();
          activeOther = activeOther + graphDataset.getRecord(j).getOtherCases();
        }
        //sum cases together
        totalActiveCases = activeStaff + activeStudents + activeOther;

        //convert current date to day in the year
        dayOfYear = graphDataset.getRecord(i).getDate().getDayOfYear();

        //add totalActiveCases for the day to the series
        series.getData().add(new XYChart.Data(dayOfYear, totalActiveCases));
      }

      //generate the scene and add the graph into it
      Scene scene = new Scene(lineChart,800,600);
      // assign the data to the graph
      lineChart.getData().add(series);
      lineChart.setCreateSymbols(false);

      //display the graph
      stage.setScene(scene);
      stage.show();
    }
    catch (IOException ioException)
    {
      out.println("IOException: " + ioException);
      System.exit(1);
    }
    catch (DatasetException datasetException)
    {
      out.println("DatasetException: " + datasetException);
      System.exit(1);
    }
  }

  public static void main(String[] args) {
    if (args.length != 1)
    {
      out.println("usage: CovidChart <input_file>");
      System.exit(1);
    }
      launch(args);
  }
}
