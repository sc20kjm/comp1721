package comp1721.cwk1;

import java.time.LocalDate;

public class CaseRecord {
    private LocalDate date;
    private int staffCases;
    private int studentCases;
    private int otherCases;

    // constructor method initialises values for variables
    public CaseRecord(LocalDate date, int staff, int student, int other)
    {
        if (staff < 0 || student < 0 || other < 0)
        {
            throw new DatasetException("Invalid values for staff, student, or other");
        }
        else
        {
            this.date = date;
            staffCases = staff;
            studentCases = student;
            otherCases = other;
        }
    }

    public LocalDate getDate()
    {
        return date;
    }

    public int getStaffCases()
    {
        return staffCases;
    }

    public int getStudentCases()
    {
        return studentCases;
    }

    public int getOtherCases()
    {
        return otherCases;
    }

  // returns the total number of COVID cases recorded in a CaseRecord
  public int totalCases()
  {
      return staffCases + studentCases + otherCases;
  }

  //returns a string containing all the field values from the CaseRecord
  public String toString()
  {
      return date + ": " + staffCases + " staff, " +
          studentCases + " students, " + otherCases + " other";
  }
}
