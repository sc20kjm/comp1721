package comp1721.cwk1;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class CovidDataset {

    private ArrayList<CaseRecord> dataset;

    // constructor method to initialise dataset arraylist
    public CovidDataset()
    {
      dataset = new ArrayList<>();
    }

    // returns the number of CaseRecord objects stored in dataset
    public int size()
    {
        return dataset.size();
    }

    /*
    returns the CaseRecord object stored in the provided index.
    throws DatasetException if index is invalid
     */
    public CaseRecord getRecord(int index)
    {
      if (index < 0 || index >= dataset.size())
      {
        throw new DatasetException ("Index supplied is invalid");
      }
      else
      {
        return dataset.get(index);
      }
    }

    // appends a new CaseRecord to the end of dataset
    public void addRecord(CaseRecord rec)
    {
      dataset.add(rec);
    }

    /*
    returns the CaseRecord on the given day.
    if no CaseRecord is found, it should inform the user
     */
    public CaseRecord dailyCasesOn(LocalDate day)
    {
      CaseRecord iteration;

      //iterate through all dataset
      for (CaseRecord caseRecord : dataset)
      {
        // assign the current CaseRecord in the list
        iteration = caseRecord;
        // check if the dates match
        if (iteration.getDate().compareTo(day) == 0)
        {
          // return the case record if the dates match
          return caseRecord;
        }
      }
      // throw exception if no CaseRecord is returned
      throw new DatasetException("No CaseRecord found with the given date");
    }

    /*
    clears the current dataset before reading data from the filename provided and
    adding to dataset
     */
    public void readDailyCases(String filename) throws IOException
    {
      // array to hold each part of the record for a line in the file
      LocalDate date;
      int staff;
      int students;
      int other;
      String splitBy = ",";

      // clear existing dataset
      dataset.clear();

      // create new scanner object to read file
      try (Scanner input = new Scanner(Paths.get(filename)))
      {
        // skip headers
        input.nextLine();

        // iterate through the file until EOF
        while (input.hasNextLine())
        {
          // store the line and split into parts
          String line = input.nextLine();
          String[] record = line.split(splitBy);

          // check if the record has the correct number of columns
          if (record.length != 4)
          {
            throw new DatasetException("A record has the incorrect number of columns");
          }

          // convert the data to correct types
          date = LocalDate.parse(record[0]);
          staff = Integer.parseInt(record[1]);
          students = Integer.parseInt(record[2]);
          other = Integer.parseInt(record[3]);

          // create new case record to add the data to
          CaseRecord newRecord = new CaseRecord(date, staff, students, other);
          // then add the new record to dataset
          dataset.add(newRecord);
        }
      }
    }

    /*
    counts the number of active cases for staff, students and other
    then writes the data to the given filename
     */
    public void writeActiveCases(String filename) throws IOException
    {
      int activeStaff;
      int activeStudents;
      int activeOther;

      // check there are at least 10 CaseRecord objects in the dataset
      if (dataset.size() < 10)
      {
        throw new DatasetException("Not enough case records to write to the file");
      }

      // create writer
      Path path = Paths.get(filename);

      try (PrintWriter out = new PrintWriter(Files.newBufferedWriter(path)))
      {
        // print headers to the file
        out.println("Date,Staff,Students,Other");

        // iterate through each dataset, skipping the first 9 days
        for (int i = 9; i < dataset.size(); i++)
        {
          // reset values for active cases
          activeStaff = 0;
          activeStudents = 0;
          activeOther = 0;

          // iterate through the past 10 days
          for (int j = i; j > (i - 10); j--)
          {
            // sum the active cases
            activeStaff = activeStaff + dataset.get(j).getStaffCases();
            activeStudents = activeStudents + dataset.get(j).getStudentCases();
            activeOther = activeOther + dataset.get(j).getOtherCases();
          }
          // write the active cases as a line to the file
          out.printf("%s,%d,%d,%d\n", dataset.get(i).getDate().toString(), activeStaff, activeStudents, activeOther);
        }
      }
    }
}
