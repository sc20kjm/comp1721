package comp1721.cwk1;

import java.io.IOException;

import static java.lang.System.out;

public class ActiveCases {
  public static void main(String[] args)
  {
    if (args.length != 2)
    {
      out.println("Usage: ActiveCases <input_file> <output_file>");
      System.exit(1);
    }
    try
    {
      CovidDataset newDataset = new CovidDataset();
      newDataset.readDailyCases(args[0]);
      newDataset.writeActiveCases(args[1]);
    }
    catch (IOException ioException)
    {
      out.println("IOException: " + ioException);
      System.exit(1);
    }
    catch (DatasetException datasetException)
    {
      out.println("DatasetException: " + datasetException);
      System.exit(1);
    }
  }
}
